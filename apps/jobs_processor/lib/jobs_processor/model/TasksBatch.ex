defmodule JobsProcessor.Model.TaskBatch do
  use Ecto.Schema
  import Ecto.Changeset
  alias JobsProcessor.Model.Task

  schema "tasks" do
    embeds_many(:tasks, Task)
  end

  def changeset(%__MODULE__{} = tasks_batch, params \\ %{}) do
    tasks_batch
    |> cast(params, [])
    |> cast_embed(:tasks)
  end
end
