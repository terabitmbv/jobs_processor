defmodule JobsProcessor.Model.Task do
  use Ecto.Schema
  import Ecto.Changeset

  embedded_schema do
    field(:command, :string)
    field(:name, :string)
    field(:requires, {:array, :string})
  end

  @doc false
  def changeset(%__MODULE__{} = task, attrs) do
    task
    |> cast(attrs, [:name, :command, :requires])
    |> validate_required([:name, :command])
  end
end
