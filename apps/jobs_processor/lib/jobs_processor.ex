defmodule JobsProcessor do
  @moduledoc """
  JobsProcessor keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  alias JobsProcessor.Model.Task

  @spec sort_tasks_by_dependency([%Task{}]) :: {:error, String.t()} | {:ok, [String.t()]}
  def sort_tasks_by_dependency(tasks) do
    tasks
    |> Enum.reduce(%{}, fn task, acc -> Map.put(acc, task.name, task) end)
    |> Map.to_list()
    |> Enum.map(fn {k, v} ->
      case Map.has_key?(v, :requires) do
        true ->
          {k, v.requires}

        false ->
          {k, []}
      end
    end)
    |> sort_by_dependency()
  end

  defp sort_by_dependency(tasks) do
    g = :digraph.new()

    Enum.each(tasks, fn {l, deps} ->
      # noop if task already added
      :digraph.add_vertex(g, l)
      Enum.each(deps, fn d -> add_dependency(g, l, d) end)
    end)

    if t = :digraph_utils.topsort(g) do
      :digraph.delete(g)
      {:ok, t}
    else
      :digraph.delete(g)
      {:error, "Unsortable contains circular dependencies"}
    end
  end

  defp add_dependency(_g, l, l), do: :ok

  defp add_dependency(g, l, d) do
    # noop if dependency already added
    :digraph.add_vertex(g, d)
    # Dependencies represented as an edge d -> l
    :digraph.add_edge(g, d, l)
  end
end
