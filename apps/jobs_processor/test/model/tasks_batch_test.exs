defmodule JobsProcessor.Model.TaskBatchTest do
  use ExUnit.Case, async: true

  alias JobsProcessor.Model.TaskBatch

  @task_attrs %{
    name: "some_name",
    command: "some_command",
    requires: []
  }

  @valid_attrs %{
    tasks: [@task_attrs, @task_attrs]
  }

  test "valid params" do
    changeset = TaskBatch.changeset(%TaskBatch{}, @valid_attrs)
    assert changeset.valid? == true
  end
end
