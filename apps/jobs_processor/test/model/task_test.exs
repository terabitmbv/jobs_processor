defmodule JobsProcessor.Model.TaskTest do
  use ExUnit.Case, async: true

  alias JobsProcessor.Model.Task

  @valid_attrs %{
    name: "some_name",
    command: "some_command",
    requires: []
  }

  test "valid task params" do
    params = @valid_attrs
    change = Task.changeset(%Task{}, params)
    assert change.valid? == true
  end

  test "name is required" do
    params = %{@valid_attrs | name: nil}
    change = Task.changeset(%Task{}, params)
    assert change.valid? == false
    assert true == Keyword.has_key?(change.errors, :name)
  end

  test "command is required" do
    params = %{@valid_attrs | command: nil}
    change = Task.changeset(%Task{}, params)
    assert change.valid? == false
    assert true == Keyword.has_key?(change.errors, :command)
  end

  test "requires is NOT required" do
    params = Map.delete(@valid_attrs, :requires)
    change = Task.changeset(%Task{}, params)
    assert change.valid? == true
  end
end
