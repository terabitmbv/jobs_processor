defmodule JobsProcessor.JobsProcessorContextTest do
  use ExUnit.Case, async: true

  alias JobsProcessor.Model.Task

  @tasks [
    %Task{
      name: "task1",
      command: "some_command",
      requires: ["task2"]
    },
    %Task{
      name: "task2",
      command: "some_command",
      requires: []
    }
  ]

  test "sort tasks by dependencies" do
    assert {:ok, ["task2", "task1"]} == JobsProcessor.sort_tasks_by_dependency(@tasks)
  end

  # TODO: "implement the following test"
  # test "sort tasks containing a dependency to non existing task" do

  #   assert {:error,_} == JobsProcessor.sort_tasks_by_dependency([%Task{name: "x", command: "xx" , requires: ["vv"]} | @tasks])
  # end
end
