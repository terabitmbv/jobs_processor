defmodule JobsProcessorWeb.TasksView do
  use JobsProcessorWeb, :view
  alias JobsProcessorWeb.TasksView

  def render("index.json", %{users: users}) do
    %{data: render_many(users, TasksView, "tasks.json")}
  end

  def render("show.json", %{tasks: tasks}) do
    %{data: render_one(tasks, TasksView, "tasks.json")}
  end

  def render("tasks.json", %{tasks: tasks}) do
    %{id: tasks.id, name: tasks.name}
  end
end
