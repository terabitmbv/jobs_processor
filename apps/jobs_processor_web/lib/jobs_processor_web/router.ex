defmodule JobsProcessorWeb.Router do
  use JobsProcessorWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", JobsProcessorWeb do
    pipe_through :api

    post "/index", JobsProcessorWeb.TasksController, :index
    post "/to_script", JobsProcessorWeb.TasksController, :to_script
  end
end
