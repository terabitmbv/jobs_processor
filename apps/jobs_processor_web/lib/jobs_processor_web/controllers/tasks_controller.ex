defmodule JobsProcessorWeb.JobsProcessorWeb.TasksController do
  use JobsProcessorWeb, :controller

  alias JobsProcessor.Model.TaskBatch

  def index(conn, %{"tasks" => _t} = params) do
    changeset = TaskBatch.changeset(%TaskBatch{}, params)

    case changeset.valid? do
      true ->
        result =
          changeset.changes.tasks
          |> Enum.map(fn cs -> %{name: cs.changes.name, command: cs.changes.command} end)

        json(conn, result)

      _ ->
        send_422(conn, "unprocessable data")
    end
  end

  def index(conn, _params) do
    send_422(conn, "unprocessable data")
  end

  @spec to_script(Plug.Conn.t(), any) :: Plug.Conn.t()
  def to_script(conn, %{"tasks" => _t} = params) do
    changeset = TaskBatch.changeset(%TaskBatch{}, params)

    case changeset.valid? do
      false ->
        send_422(conn, "unprocessable data")

      true ->
        result =
          changeset.changes.tasks
          |> Enum.map(fn cs -> cs.changes.command end)
          |> Enum.join(";")

        html(conn, result)
    end
  end

  def to_script(conn, _params) do
    send_422(conn, "tasks is missing")
  end

  defp send_422(conn, message) do
    conn
    |> put_status(422)
    |> json(message)
  end
end
