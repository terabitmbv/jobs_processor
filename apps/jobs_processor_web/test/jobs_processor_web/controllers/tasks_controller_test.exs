defmodule JobsProcessorWeb.TasksControllerTest do
  use JobsProcessorWeb.ConnCase

  alias JobsProcessorWeb.JobsProcessorWeb.TasksController

  @task_1_attrs %{
    "name" => "task1",
    "command" => "task1 command",
    "requires" => ""
  }

  @task_2_attrs %{
    "name" => "task2",
    "command" => "task2 command",
    "requires" => ""
  }

  @tasks_batch_attrs %{
    "tasks" => [@task_1_attrs, @task_2_attrs]
  }

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "process index winth valid data", %{conn: conn} do
    conn = TasksController.index(conn, @tasks_batch_attrs)

    assert [
             %{"name" => "task1", "command" => "task1 command"},
             %{"name" => "task2", "command" => "task2 command"}
           ] = json_response(conn, 200)
  end

  test "process empty input in index", %{conn: conn} do
    conn = TasksController.index(conn, %{})
    assert conn.status == 422
  end

  test "process malformed input in index", %{conn: conn} do
    tasks = [%{"name" => "malformed"} | @tasks_batch_attrs["tasks"]]
    conn = TasksController.index(conn, %{@tasks_batch_attrs | "tasks" => tasks})
    assert conn.status == 422
  end

  test "process to_script with valid data", %{conn: conn} do
    conn = TasksController.to_script(conn, @tasks_batch_attrs)

    assert conn.status == 200
    assert conn.resp_body == "task1 command;task2 command"
  end

  test "process to_script with empty data", %{conn: conn} do
    conn = TasksController.to_script(conn, %{})
    assert conn.status == 422
  end

  test "process to_script with malformed data", %{conn: conn} do
    tasks = [%{"name" => "malformed"} | @tasks_batch_attrs["tasks"]]
    conn = TasksController.to_script(conn, %{@tasks_batch_attrs | "tasks" => tasks})
    assert conn.status == 422
  end
end
